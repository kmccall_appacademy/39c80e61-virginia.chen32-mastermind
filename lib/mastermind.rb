PEGS = {"r"=>"red","g"=>"green","b"=>"blue","y"=>"yellow",
  "o"=>"orange","p"=>"purple"}

class Code

  def initialize(pegs)
    @pegs = pegs
  end

  attr_reader :pegs

  def self.random
    code = []
    while code.length < 4
      code.push(PEGS.values[Random.rand(5)])
    end
    return Code.new(code)
  end

  def self.parse(input)
    if colors_exist?(input)
      keys = input.downcase.chars
      values = []
      keys.each do |key|
        values.push(PEGS[key])
      end
    else raise Exception.new("Colors don't exist")
    end
  return Code.new(values)
  end

  def self.colors_exist?(input)
    colors = input.downcase.chars
    colors.each do |color|
      if PEGS.keys.include?(color) == false
        return false
      end
    end
    return true
  end

  def won?(other_code)
    return true if self==other_code
  end

  def exact_matches(other_code)
    exact_matches = 0
    index = 0
    while index < 4
      if other_code.pegs[index] == self.pegs[index]
        exact_matches+=1
      end
      index+=1
    end
    exact_matches
  end

  def near_matches(other_code)
    near_matches = 0
    used_pegs = []
    other_code.pegs.each do |peg|
      if self.pegs.include?(peg) && !(used_pegs.include?(peg))
        near_matches += [self.pegs.count(peg),
        other_code.pegs.count(peg)].min
        used_pegs.push(peg)
      end
    end
    near_matches-exact_matches(other_code)
  end

  def [](i)
    self.pegs[i]
  end

  def ==(code)
    return false if !(code.instance_of? Code)
    code.pegs.each_with_index do |peg,idx|
      if peg!=self.pegs[idx]
        return false
      end
    end
    return true
  end

end

class Game

  def initialize(secret_code=Code.random)
      @secret_code = secret_code
  end

  attr_reader :secret_code

  def get_guess
    puts "What's your guess? Format: rgrg (Possible colors: rgbyop)"
    Code.parse($stdin.gets.chomp)
  end

  def display_matches(code)
    puts "exact matches: #{code.exact_matches(secret_code)}\n"
    puts "near matches: #{code.near_matches(secret_code)}\n"
  end

  def play
    turns = 0
    puts "Welcome to Mastermind"
    new_code = []
    until secret_code.won?(new_code) || turns == 10
      new_code = self.get_guess
      self.display_matches(new_code)
      turns+=1
      puts "turns used: #{turns}"
      if secret_code.won?(new_code)
        puts "Congratulations! It took you #{turns} turns"
      end
    end
    puts "Game over, sorry you lost"
  end

end

if $PROGRAM_NAME == __FILE__
  Game.new().play
end
